package com.liujiajun.arithmetic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.liujiajun.controller")
public class ArithmeticApplication {

	public static void main(String[] args) {
		SpringApplication.run(ArithmeticApplication.class, args);
	}
}
