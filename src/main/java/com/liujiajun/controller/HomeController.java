package com.liujiajun.controller;

import com.liujiajun.Response.Question;
import com.liujiajun.util.CalculateExp;
import com.liujiajun.util.Input;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {
    @RequestMapping(value="/index")
    public String index(){
        return "home/index";
    }
    @RequestMapping(value = "/greeting")
    public String greeting(Model model){
        model.addAttribute("name","张三");
        return "home/greeting";
    }
    @ResponseBody
    @RequestMapping(value = "/getQuestion")
    public List<Question> getQuestion(){
        CalculateExp calculateExp = new CalculateExp();
        boolean flag = calculateExp.create(10,10,4);
        List<String> strings = Input.in("text.txt","javaee/operate");
        List<Question> list = new ArrayList<Question>();
        for (String str:strings) {
            String [] temp = str.split(",");
            Question question = new Question();
            question.setId(Integer.parseInt(temp[0]));
            question.setQuestion(temp[1]);
            question.setAnswer(temp[2]);
            list.add(question);
        }
        return list;
    }

    @ResponseBody
    @RequestMapping(value = "/getAnswer")
    public List<Boolean> getAnswer(){
        return null;
    }
    /**
     * idea快捷键 main => psvm
     * for =>fori
     * system.out.println => sout
     */
}
