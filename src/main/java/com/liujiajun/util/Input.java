package com.liujiajun.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Input {
    public static List<String> in (String textName,String path){
        List<String> list = null;
        try{
            FileReader fileReader = new FileReader("G://"+path+"/"+textName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = "";
            list = new ArrayList<String>();
            while ((line=bufferedReader.readLine())!=null){
                list.add(line);
            }
            bufferedReader.close();
            fileReader.close();
        }catch (IOException e){
            e.printStackTrace();
        }
        return list;
    }
}
